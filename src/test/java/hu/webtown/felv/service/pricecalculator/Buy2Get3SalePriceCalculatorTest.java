package hu.webtown.felv.service.pricecalculator;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import com.google.common.collect.ImmutableMap;
import hu.webtown.felv.service.pricecalculator.Buy2Get3SalePriceCalculator;
import hu.webtown.felv.service.pricecalculator.SalePriceCalculator;
import org.junit.Before;
import org.junit.Test;

public class Buy2Get3SalePriceCalculatorTest {

  private SalePriceCalculator priceCalculator;

  @Before
  public void setUp() throws Exception {
    priceCalculator = new Buy2Get3SalePriceCalculator();
  }


  @Test
  public void shouldReducePriceIf3ItemsAreBought() throws Exception {
    whenBuying(3, "chestnut").thenReducedPriceIs(2_000);
  }

  @Test
  public void shouldNotReducePriceIf2ItemsAreBought() throws Exception {
    whenBuying(2, "chestnut").thenReducedPriceIs(2_000);
  }

  @Test
  public void shouldReducePriceIf4ItemsAreBought() throws Exception {
    whenBuying(4, "chestnut").thenReducedPriceIs(3_000);
  }

  @Test
  public void shouldReducePriceIf6ItemsAreBought() throws Exception {
    whenBuying(6, "chestnut").thenReducedPriceIs(4_000);
  }

  @Test
  public void typeOfSale() throws Exception {
    assertThat(priceCalculator.getTypeOfSale(),is("buy 2 get 3"));
  }

  private Tester whenBuying(int number, String productName) {
    return new Tester(number, productName);
  }

  private class Tester {

    private int number;
    private String productName;

    private Tester(int number, String productName) {
      this.number = number;
      this.productName = productName;
    }

    private void thenReducedPriceIs(int expectedPrice) {
      //when
      int actual = priceCalculator.calculatePriceFor(ImmutableMap.of(productName, number));

      //then
      assertThat(actual, is(expectedPrice));
    }
  }
}