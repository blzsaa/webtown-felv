package hu.webtown.felv.service.pricecalculator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import com.google.common.collect.ImmutableMap;
import hu.webtown.felv.service.pricecalculator.MegaPackSalePriceCalculator;
import org.junit.Before;
import org.junit.Test;

public class MegaPackSalePriceCalculatorTest {
  private MegaPackSalePriceCalculator priceCalculator;

  @Before
  public void setUp() throws Exception {
    priceCalculator = new MegaPackSalePriceCalculator();
  }

  @Test
  public void aMegaPackShouldReducePriceBy6000() throws Exception {
    whenBuying(12, "chestnut").thenMegaPackPriceShouldBe(6_000);
  }

  @Test
  public void twoMegaPackShouldReducePriceBy12000() throws Exception {
    whenBuying(24, "chestnut").thenMegaPackPriceShouldBe(12_000);
  }

  @Test
  public void cannotBuyRubberDuckWithMegaPack() throws Exception {
    whenBuying(12, "rubberDuck").thenMegaPackPriceShouldBe(36_000);
  }

  @Test
  public void cannotBuyTeliszalamiWithMegaPack() throws Exception {
    whenBuying(12, "teliszalami").thenMegaPackPriceShouldBe(24_000);
  }

  @Test
  public void canBuyCucumberWithMegaPack() throws Exception {
    whenBuying(12, "cucumber").thenMegaPackPriceShouldBe(27_600);
  }


  @Test
  public void shouldNotifyWhenProductIsUnknown() throws Exception {
    // when
    Throwable thrown = catchThrowable(
        () -> priceCalculator.calculatePriceFor(ImmutableMap.of("unknown", 1)));

    //then
    assertThat(thrown).isInstanceOf(IllegalStateException.class)
        .hasMessage("unknown product: unknown");
  }

  @Test
  public void typeOfSale() throws Exception {
    assertThat(priceCalculator.getTypeOfSale()).isEqualTo("mega pack");
  }

  private Tester
  whenBuying(int number, String productName) {
    return new Tester(number, productName);
  }

  private class Tester {

    private int number;
    private String productName;

    private Tester(int number, String productName) {
      this.number = number;
      this.productName = productName;
    }
    private void thenMegaPackPriceShouldBe(int expectedPrice) {
      //when
      int actual = priceCalculator.calculatePriceFor(ImmutableMap.of(productName, number));

      //then
      assertThat(actual).isEqualTo(expectedPrice);
    }
  }
}