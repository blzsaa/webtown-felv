package hu.webtown.felv.service.pricecalculator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import com.google.common.collect.ImmutableMap;
import hu.webtown.felv.service.pricecalculator.NoSalePriceCalculator;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class NoSalePriceCalculatorTest {
  private NoSalePriceCalculator priceCalculator;

  @Before
  public void setUp() throws Exception {
    priceCalculator = new NoSalePriceCalculator();
  }

  @Test
  public void shouldCorrectlyCalculateOriginalPriceOfChestnut() throws Exception {
    whenBuying(6, "chestnut").thenOriginalPriceIs(6_000);
  }

  @Test
  public void shouldCorrectlyCalculateOriginalPriceOfTeliszalami() throws Exception {
    whenBuying(6, "Teliszalami").thenOriginalPriceIs(12_000);
  }

  @Test
  public void shouldCorrectlyCalculateOriginalPriceOfCucumber() throws Exception {
    whenBuying(6, "Cucumber").thenOriginalPriceIs(16_800);
  }

  @Test
  public void shouldCorrectlyCalculateOriginalPriceOfRubberDuck() throws Exception {
    whenBuying(6, "RubberDuck").thenOriginalPriceIs(18_000);
  }

  @Test
  public void shouldNotifyWhenProductIsUnknown() throws Exception {
    // when
    Throwable thrown = catchThrowable(
        () -> priceCalculator.calculatePriceFor(ImmutableMap.of("unknown", 1)));

    //then
    assertThat(thrown).isInstanceOf(IllegalStateException.class)
        .hasMessage("unknown product: unknown");
  }

  @Test
  public void typeOfSale() throws Exception {
    Assert.assertThat(priceCalculator.getTypeOfSale(),is("no sale"));
  }

  private ProductPriceReaderTester whenBuying(int number, String productName) {
    return new ProductPriceReaderTester(number, productName);
  }

  private class ProductPriceReaderTester {

    private int number;
    private String productName;

    private ProductPriceReaderTester(int number, String productName) {
      this.number = number;
      this.productName = productName;
    }

    private void thenOriginalPriceIs(int expectedOriginalPrice) {
      //when
      int actual = priceCalculator.calculatePriceFor(ImmutableMap.of(productName, number));

      //then
      Assert.assertThat(actual, is(expectedOriginalPrice));
    }
  }
}