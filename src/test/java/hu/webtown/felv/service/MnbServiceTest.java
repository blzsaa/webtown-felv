package hu.webtown.felv.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;

import hu.mnb.arfolyamok.GetCurrentExchangeRatesResponseBody;
import java.text.ParseException;
import javax.xml.bind.JAXBElement;
import org.assertj.core.api.Assertions;
import org.assertj.core.data.Percentage;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ws.client.core.WebServiceTemplate;

public class MnbServiceTest {

  private static final String FT_2_EURO_RATE = "300,00";

  @Mock
  private WebServiceTemplate webServiceTemplate;
  @Mock
  private JAXBElement jaxbResponse;
  @Mock
  private GetCurrentExchangeRatesResponseBody getCurrentExchangeRatesResponseBody;
  @Mock
  private JAXBElement innerJaxbElement;
  private MnbService mnbService;

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    mnbService = new MnbService(webServiceTemplate);
  }

  @Test
  public void shouldConvert300FtTo1Euro() {
    //given
    mockSoapService("300,00");

    //when
    double priceInEuro = mnbService.convertFt2Euro(300);

    //then
    assertThat(priceInEuro).isCloseTo(1d, Percentage.withPercentage(0.1d));
  }

  private void mockSoapService(String ft2EuroRate) {
    doReturn(jaxbResponse).when(webServiceTemplate).marshalSendAndReceive(any(JAXBElement.class));
    doReturn(getCurrentExchangeRatesResponseBody).when(jaxbResponse).getValue();
    doReturn(innerJaxbElement).when(getCurrentExchangeRatesResponseBody)
        .getGetCurrentExchangeRatesResult();
    doReturn("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
        + "<MNBCurrentExchangeRates>\n"
        + "   <Day date=\"2017-09-12\">\n"
        + "      <Rate unit=\"1\" curr=\"EUR\">" + ft2EuroRate + "</Rate>\n"
        + "   </Day>\n"
        + "</MNBCurrentExchangeRates>").when(innerJaxbElement).getValue();
  }

  @Test
  public void shouldConvert150FtToHalfEuro() {
    //given
    mockSoapService(FT_2_EURO_RATE);

    //when
    double priceInEuro = mnbService.convertFt2Euro(150);

    //then
    assertThat(priceInEuro).isCloseTo(0.5d, Percentage.withPercentage(0.1d));
  }

  @Test
  public void shouldHandleExceptions() {
    //given
    mockSoapService("asd");

    Throwable thrown = catchThrowable(() -> mnbService.convertFt2Euro(150));

    //then
    Assertions.assertThat(thrown).isInstanceOf(MnbServiceException.class)
        .hasCauseInstanceOf(ParseException.class);
  }
}