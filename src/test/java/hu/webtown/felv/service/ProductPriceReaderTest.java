package hu.webtown.felv.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.doReturn;

import hu.webtown.felv.pojo.Price;
import hu.webtown.felv.service.pricecalculator.NoSalePriceCalculator;
import hu.webtown.felv.service.pricecalculator.SalePriceCalculator;
import java.util.Arrays;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.assertj.core.data.Percentage;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class ProductPriceReaderTest {

  @Mock
  private Map<String, Integer> orderList;
  @Mock
  private SalePriceCalculator calculator1;
  @Mock
  private SalePriceCalculator calculator2;
  @Mock
  private NoSalePriceCalculator noSalePriceCalculator;
  @Mock
  private MnbService mnbService;
  private ProductPriceReader reader;

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    reader = new ProductPriceReader(Arrays.asList(calculator1, calculator2), noSalePriceCalculator,
        mnbService);
  }

  @Test
  public void shouldAskForOriginalPrice() throws Exception {
    //given
    doReturn(10).when(noSalePriceCalculator).calculatePriceFor(orderList);

    //when
    int actual = reader.calculatePrice(orderList).getOriginalPrice();

    //then
    assertThat(actual).isEqualTo(10);
  }

  @Test
  public void shouldReturnForTheLowestPrice() throws Exception {
    //given
    doReturn(10).when(noSalePriceCalculator).calculatePriceFor(orderList);
    doReturn(5).when(calculator1).calculatePriceFor(orderList);
    doReturn(3).when(calculator2).calculatePriceFor(orderList);

    doReturn("asd").when(calculator2).getTypeOfSale();
    doReturn(1d).when(mnbService).convertFt2Euro(3);

    //when
    Price actual = reader.calculatePrice(orderList);

    //then
    assertThat(actual.getReducedPrice()).isEqualTo(3);
    assertThat(actual.getTypeOfSale()).isEqualTo("asd");
    assertThat(actual.getReducedPriceInEuro()).isCloseTo(1d, Percentage.withPercentage(0.1d));
  }
}