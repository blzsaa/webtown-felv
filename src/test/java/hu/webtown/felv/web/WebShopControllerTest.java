package hu.webtown.felv.web;

import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import com.google.common.collect.ImmutableMap;
import hu.webtown.felv.pojo.Price;
import hu.webtown.felv.pojo.Price.PriceBuilder;
import hu.webtown.felv.service.ProductPriceReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;

public class WebShopControllerTest {

  private WebShopController controller;
  @Mock
  private Model model;
  @Mock
  private ProductPriceReader productPriceReader;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    controller = new WebShopController(productPriceReader);
  }

  @Test
  public void frontPageShouldBeIndexHtml() throws IOException, InterruptedException {
    //when
    String actual = controller.frontPage(model);

    //then
    assertThat(actual, is("index"));
  }

  @Test
  public void shouldInitFrontPage() throws IOException, InterruptedException {
    //when
    controller.frontPage(model);

    //then
    Map<String, Integer> orderList = new TreeMap<>();
    orderList.put("rubberDuck", 0);
    orderList.put("teliszalami", 0);
    orderList.put("chestnut", 0);
    orderList.put("cucumber", 0);

    verify(model).addAttribute("orderList", orderList);
    verify(model).addAttribute("price",
        new Price.PriceBuilder().originalPrice(0).reducedPrice(0).typeOfSale("").build());
  }

  @Test
  public void shouldAskForPrice() throws Exception {
    //given
    ImmutableMap<String, Integer> orderList = ImmutableMap.of("rubberDuck", 3);
    Price price = new PriceBuilder().originalPrice(2).reducedPrice(1).typeOfSale("a").build();

    doReturn(price).when(productPriceReader).calculatePrice(orderList);

    //when
    String actual = controller.order(singletonList("rubberDuck"), singletonList("3"), model);

    //then
    verify(model).addAttribute("price", price);
    assertThat(actual, is("index"));
  }

}
