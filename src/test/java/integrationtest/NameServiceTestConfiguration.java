package integrationtest;


import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import hu.mnb.arfolyamok.GetCurrentExchangeRatesResponseBody;
import javax.xml.bind.JAXBElement;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.ws.client.core.WebServiceTemplate;

@TestConfiguration
public class NameServiceTestConfiguration {

  @Primary
  @Bean
  public WebServiceTemplate mockSoapService() {
    return mock(WebServiceTemplate.class);
  }

  public static void setForint2EuroRateTo(WebServiceTemplate webServiceTemplate, double rate) {
    JAXBElement jaxbResponse = mock(JAXBElement.class);
    GetCurrentExchangeRatesResponseBody getCurrentExchangeRatesResponseBody = mock(
        GetCurrentExchangeRatesResponseBody.class);
    JAXBElement innerJaxbElement = mock(JAXBElement.class);
    doReturn(jaxbResponse).when(webServiceTemplate).marshalSendAndReceive(any(JAXBElement.class));
    doReturn(getCurrentExchangeRatesResponseBody).when(jaxbResponse).getValue();
    doReturn(innerJaxbElement).when(getCurrentExchangeRatesResponseBody)
        .getGetCurrentExchangeRatesResult();
    doReturn("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
        + "<MNBCurrentExchangeRates>\n"
        + "   <Day date=\"2017-09-12\">\n"
        + "      <Rate unit=\"1\" curr=\"EUR\">" + rate + "</Rate>\n"
        + "   </Day>\n"
        + "</MNBCurrentExchangeRates>").when(innerJaxbElement).getValue();
  }
}