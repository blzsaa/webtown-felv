package integrationtest;

import static integrationtest.NameServiceTestConfiguration.setForint2EuroRateTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import hu.webtown.felv.FelvApplication;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ws.client.core.WebServiceTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {FelvApplication.class},
    webEnvironment = WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = {NameServiceTestConfiguration.class})
public class WebShopSeleniumIT {

  private static final Logger LOGGER = LoggerFactory.getLogger(WebShopSeleniumIT.class);
  private static final String PHANTOMJS_BINARY_PATH = "phantomjs.binary.path";
  private static WebDriver driver;
  @LocalServerPort
  private int randomServerPort;
  @Autowired
  private WebServiceTemplate webServiceTemplate;

  @BeforeClass
  public static void beforeClass() throws Exception {
    if (System.getProperty("phantomjs.binary.path") == null) {
      setBinaryPath();
    }
    driver = new PhantomJSDriver();

  }

  @AfterClass
  public static void afterClass() throws Exception {
    driver.quit();
  }


  @After
  public void tearDown() throws Exception {
    driver.manage().deleteAllCookies();
  }

  @Test
  public void frontPage() throws Exception {
    //given
    openPage();

    //then
    assertThat(driver.getTitle(), is("Welcome to our web shop!"));
  }

  private void openPage() {
    driver.get("http://localhost:" + randomServerPort);
  }

  @Test
  public void shouldUIWork() throws Exception {
    //given
    setForint2EuroRateTo(webServiceTemplate, 246);
    openPage();

    //when
    buy(2).fromRow(2);
    buy(3).fromRow(3);
    buy(4).fromRow(4);
    buy(5).fromRow(5);

    driver.findElement(By.name("order")).submit();

    //then
    assertThat(readValueOf("originalPrice"), is("32400"));
    assertThat(readValueOf("reducedPrice"), is("24600"));
    assertThat(readValueOf("reducedPriceInEuro"), is("100.0"));
    assertThat(readValueOf("typeOfSale"), is("buy 2 get 3"));
  }

  private String readValueOf(String name) {
    return driver.findElement(By.name(name + "Value")).getAttribute("value");
  }

  private Tester buy(int quantity) {
    return new Tester(quantity);
  }

  private static void setBinaryPath() {
    List<File> driverBinaries = new ArrayList<>(collectBinaries());
    verifyBinary(driverBinaries);
    String phantomJsPath = driverBinaries.get(0).getAbsolutePath();
    LOGGER.debug("Setting {} to {}", PHANTOMJS_BINARY_PATH, phantomJsPath);
    System.setProperty(PHANTOMJS_BINARY_PATH, phantomJsPath);
  }

  private static void verifyBinary(Collection<File> driverBinaries) {
    if (driverBinaries.isEmpty()) {
      throw new IllegalStateException(
          "No phantomjs.exe found, try mvn clean install to automatically download it!");
    }
  }

  private static Collection<File> collectBinaries() {
    return FileUtils.listFiles(new File("."), new IOFileFilter() {
      @Override
      public boolean accept(File file) {
        return "phantomjs.exe".equals(file.getName());
      }

      @Override
      public boolean accept(File dir, String name) {
        return "phantomjs.exe".equals(name);
      }
    }, TrueFileFilter.INSTANCE);
  }

  private class Tester {

    private int quantity;

    private Tester(int quantity) {
      this.quantity = quantity;
    }

    private void fromRow(int row) {
      driver.findElement(By.xpath("//tr[" + row + "]/td[2]/input"))
          .sendKeys(String.valueOf(quantity));
    }
  }
}
