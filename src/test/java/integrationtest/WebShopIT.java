package integrationtest;

import static integrationtest.NameServiceTestConfiguration.setForint2EuroRateTo;
import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import hu.webtown.felv.FelvApplication;
import hu.webtown.felv.pojo.Price;
import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.ws.client.core.WebServiceTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {FelvApplication.class},
    webEnvironment = WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = {NameServiceTestConfiguration.class})
public class WebShopIT {

  private MockMvc mvc;
  @Autowired
  private WebApplicationContext context;
  @Autowired
  private WebServiceTemplate webServiceTemplate;


  @Before
  public void setup() {
    setForint2EuroRateTo(webServiceTemplate,300);
    mvc = MockMvcBuilders.webAppContextSetup(context).build();
  }

  @Test
  public void frontPageAppears() throws Exception {
    //when
    ResultActions resultActions = mvc.perform(get("/"));

    //then
    resultActions
        .andExpect(content().string(containsString("Welcome to our web shop!")))
        .andExpect(status().isOk());
  }

  @Test
  public void shouldSaveOrders() throws Exception {
    //when
    ResultActions actual = mvc.perform(post("/order")
        .param("productNames[]", "rubberDuck", "teliszalami", "chestnut", "cucumber")
        .param("quantities[]", "2", "2", "2", "2"));

    //then
    Map<String, Integer> orderList = new HashMap<>();

    orderList.put("rubberDuck", 2);
    orderList.put("teliszalami", 2);
    orderList.put("chestnut", 2);
    orderList.put("cucumber", 2);

    actual.andExpect(model().attribute("orderList", orderList));
  }

  @Test
  public void shouldCalculateBuy2Get3Prices() throws Exception {
    //given
    setForint2EuroRateTo(webServiceTemplate,316d);

    //when
    ResultActions actual = mvc.perform(post("/order")
        .param("productNames[]", "rubberDuck", "teliszalami", "chestnut", "cucumber")
        .param("quantities[]", "3", "3", "3", "10"));

    //then
    Price p = new Price.PriceBuilder().originalPrice(46_000).reducedPrice(31_600)
        .reducedPriceInEuro(100)
        .typeOfSale("buy 2 get 3").build();
    actual.andExpect(model().attribute("price", p));
  }

  @Test
  public void shouldCalculateMegaPack() throws Exception {
    //given
    setForint2EuroRateTo(webServiceTemplate, 300d);

    //when
    ResultActions actual = mvc.perform(post("/order")
        .param("productNames[]", "chestnut")
        .param("quantities[]", "12"));

    //then
    Price p = new Price.PriceBuilder().originalPrice(12_000).reducedPrice(6_000)
        .reducedPriceInEuro(20)
        .typeOfSale("mega pack").build();
    actual.andExpect(model().attribute("price", p));
  }

}
