package hu.webtown.felv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FelvApplication {

	public static void main(String[] args) {
		SpringApplication.run(FelvApplication.class, args);
	}
}
