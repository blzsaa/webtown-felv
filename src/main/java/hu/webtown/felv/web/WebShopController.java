package hu.webtown.felv.web;

import hu.webtown.felv.pojo.Price;
import hu.webtown.felv.service.ProductPriceReader;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class WebShopController {

  private static final String ORDER_LIST = "orderList";
  private static final String PRICE = "price";

  private ProductPriceReader productPriceReader;

  @Autowired
  public WebShopController(ProductPriceReader productPriceReader) {
    this.productPriceReader = productPriceReader;
  }


  @RequestMapping("/")
  public String frontPage(Model model) {
    if (!model.containsAttribute(ORDER_LIST)) {
      Map<String, Integer> orderList = new TreeMap<>();
      orderList.put("rubberDuck", 0);
      orderList.put("teliszalami", 0);
      orderList.put("chestnut", 0);
      orderList.put("cucumber", 0);
      model.addAttribute(ORDER_LIST, orderList);
    }
    if (!model.containsAttribute(PRICE)) {
      model.addAttribute(PRICE, new Price.PriceBuilder().originalPrice(0).reducedPrice(0).typeOfSale("").build());
    }
    return "index";
  }

  @PostMapping("/order")
  public String order(
      @RequestParam(value = "productNames[]") List<String> productNames,
      @RequestParam(value = "quantities[]") List<String> quantities, Model model) {

    Map<String, Integer> orderList = mergeLists2Map(productNames, quantities);

    model.addAttribute(ORDER_LIST, orderList);
    model.addAttribute(PRICE, productPriceReader.calculatePrice(orderList));
    return "index";
  }

  private Map<String, Integer> mergeLists2Map(List<String> productNames, List<String> quantities) {
    Map<String, Integer> map = new TreeMap<>();
    Iterator<String> i1 = productNames.iterator();
    Iterator<String> i2 = quantities.iterator();
    while (i1.hasNext() && i2.hasNext()) {
      map.put(i1.next(), Integer.valueOf("" + i2.next()));
    }
    return map;
  }

}
