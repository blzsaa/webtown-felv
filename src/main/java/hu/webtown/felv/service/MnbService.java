package hu.webtown.felv.service;

import hu.mnb.arfolyamok.GetCurrentExchangeRatesRequestBody;
import hu.mnb.arfolyamok.GetCurrentExchangeRatesResponseBody;
import hu.mnb.arfolyamok.ObjectFactory;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import javax.xml.bind.JAXBElement;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

@Service
public class MnbService {

  private static final String FIND_EURO_RATE_XPATH_EXPRESSION = "/MNBCurrentExchangeRates/Day/Rate[@curr=\"EUR\"]";

  private WebServiceTemplate webServiceTemplate;

  @Autowired
  public MnbService(WebServiceTemplate webServiceTemplate) {
    this.webServiceTemplate = webServiceTemplate;
  }

  public double convertFt2Euro(int priceInFt) {

    String currentExchangeRates = getCurrentExchangeRates();
    double ft2EuroRate = readFt2EuroExchangeRate(currentExchangeRates);

    return ((double) priceInFt) / ft2EuroRate;
  }

  private double readFt2EuroExchangeRate(String currentExchangeRates) {
    XPath xPath = XPathFactory.newInstance().newXPath();

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    try (StringReader reader = new StringReader(currentExchangeRates)) {
      DocumentBuilder builder = factory.newDocumentBuilder();
      Document document = builder.parse(new InputSource(reader));
      String ft2EuroRateWithCommaAsDecimalMark = (String) xPath.compile(FIND_EURO_RATE_XPATH_EXPRESSION)
          .evaluate(document, XPathConstants.STRING);

      NumberFormat formatter = new DecimalFormat("#0,00");
      Number parse = formatter.parse(ft2EuroRateWithCommaAsDecimalMark);
      return parse.doubleValue();
    } catch (Exception e) {
      throw new MnbServiceException(e);
    }

  }

  private String getCurrentExchangeRates() {
    ObjectFactory objectFactory = new ObjectFactory();

    JAXBElement<GetCurrentExchangeRatesRequestBody> request = objectFactory
        .createGetCurrentExchangeRates(objectFactory.createGetCurrentExchangeRatesRequestBody());
    JAXBElement jaxbElement = (JAXBElement) webServiceTemplate.marshalSendAndReceive(request);
    JAXBElement<String> getCurrentExchangeRatesResult = ((GetCurrentExchangeRatesResponseBody) jaxbElement
        .getValue()).getGetCurrentExchangeRatesResult();
    return getCurrentExchangeRatesResult.getValue();
  }
}
