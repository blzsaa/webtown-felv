package hu.webtown.felv.service;

import hu.webtown.felv.pojo.Price;
import hu.webtown.felv.service.pricecalculator.NoSalePriceCalculator;
import hu.webtown.felv.service.pricecalculator.SalePriceCalculator;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductPriceReader {

  private List<SalePriceCalculator> priceCalculators;
  private NoSalePriceCalculator noSalePriceCalculator;
  private MnbService mnbService;
  private int originalPrice;

  @Autowired
  public ProductPriceReader(
      List<SalePriceCalculator> priceCalculators,
      NoSalePriceCalculator noSalePriceCalculator,
      MnbService mnbService) {
    this.priceCalculators = priceCalculators;
    this.noSalePriceCalculator = noSalePriceCalculator;
    this.mnbService = mnbService;
  }

  public Price calculatePrice(Map<String, Integer> map) {
    SalePriceCalculator best = findBestSale(map);

    originalPrice = calculateOriginalPrice(map);
    int reducedPrice = best.calculatePriceFor(map);
    String typeOfSale = best.getTypeOfSale();
    double reducedPriceInEuro = mnbService.convertFt2Euro(reducedPrice);

    return new Price.PriceBuilder()
        .originalPrice(originalPrice)
        .reducedPrice(reducedPrice)
        .reducedPriceInEuro(reducedPriceInEuro)
        .typeOfSale(typeOfSale).build();
  }

  private SalePriceCalculator findBestSale(Map<String, Integer> orderList) {
    return priceCalculators.stream().min(
        Comparator.comparingInt(c -> c.calculatePriceFor(orderList)))
        .orElseThrow(() -> new IllegalStateException("problem with price calculators"));
  }

  private int calculateOriginalPrice(Map<String, Integer> map) {
    return noSalePriceCalculator.calculatePriceFor(map);
  }

}
