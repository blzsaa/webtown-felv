package hu.webtown.felv.service.pricecalculator;

import java.util.Map;
import java.util.Map.Entry;
import org.springframework.stereotype.Service;

@Service
public class Buy2Get3SalePriceCalculator extends SalePriceCalculator {

  @Override
  public int calculatePriceFor(Map<String, Integer> orderList) {
    int sumPrice = 0;
    for (Entry<String, Integer> order : orderList.entrySet()) {
      sumPrice += getReducedPriceByBuy2Get3(order.getKey(), order.getValue());
    }
    return sumPrice;
  }

  @Override
  public String getTypeOfSale() {
    return "buy 2 get 3";
  }

  private int getReducedPriceByBuy2Get3(String productName, Integer quantity) {
    int freeItems = quantity / 3;
    return getOriginalPriceOf(productName, quantity - freeItems);
  }
}
