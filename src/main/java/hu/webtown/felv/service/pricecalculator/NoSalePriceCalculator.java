package hu.webtown.felv.service.pricecalculator;

import java.util.Map;
import java.util.Map.Entry;
import org.springframework.stereotype.Service;

@Service
public class NoSalePriceCalculator extends SalePriceCalculator {

  @Override
  public int calculatePriceFor(Map<String, Integer> orderList) {
    int sumPrice = 0;
    for (Entry<String, Integer> order : orderList.entrySet()) {
      sumPrice += getOriginalPriceOf(order.getKey(), order.getValue());
    }
    return sumPrice;
  }

  @Override
  public String getTypeOfSale() {
    return "no sale";
  }

}
