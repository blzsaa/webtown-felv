package hu.webtown.felv.service.pricecalculator;

import java.util.Map;
import java.util.Map.Entry;
import org.springframework.stereotype.Service;

@Service
public class MegaPackSalePriceCalculator extends SalePriceCalculator {

  @Override
  public int calculatePriceFor(Map<String, Integer> orderList) {
    int sumPrice = 0;
    for (Entry<String, Integer> order : orderList.entrySet()) {
      sumPrice += getReducedPriceByMegaPack(order.getKey(), order.getValue());
    }
    return sumPrice;
  }

  @Override
  public String getTypeOfSale() {
    return "mega pack";
  }

  private int getReducedPriceByMegaPack(String productName, Integer quantity) {
    if (isMegaPackSaleValidForThiaProduct(productName)) {
      int numberOfMegaPacks = quantity / 12;
      return getOriginalPriceOf(productName, quantity) - numberOfMegaPacks * 6_000;
    } else {
      return getOriginalPriceOf(productName, quantity);
    }
  }

  private boolean isMegaPackSaleValidForThiaProduct(String productName) {
    switch (productName.toLowerCase()) {
      case "chestnut":
        return true;
      case "teliszalami":
        return false;
      case "cucumber":
        return true;
      case "rubberduck":
        return false;
      default:
        throw new IllegalStateException("unknown product: " + productName);
    }
  }
}
