package hu.webtown.felv.service.pricecalculator;

import java.util.Map;

public abstract class SalePriceCalculator {

  public abstract int calculatePriceFor(Map<String, Integer> orderList);

  public abstract String getTypeOfSale();
  
  protected int getOriginalPriceOf(String productName, int number) {
    return getPriceOf(productName) * number;
  }

  private int getPriceOf(String productName) {
    switch (productName.toLowerCase()) {
      case "chestnut":
        return 1000;
      case "teliszalami":
        return 2000;
      case "cucumber":
        return 2800;
      case "rubberduck":
        return 3000;
      default:
        throw new IllegalStateException("unknown product: " + productName);
    }
  }


}
