package hu.webtown.felv.service;

public class MnbServiceException extends RuntimeException {

  public MnbServiceException(Throwable throwable) {
    super(throwable);
  }
}
