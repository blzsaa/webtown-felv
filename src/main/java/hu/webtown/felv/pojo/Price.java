package hu.webtown.felv.pojo;

public class Price {

  private int originalPrice;
  private int reducedPrice;
  private String typeOfSale;
  private double reducedPriceInEuro;

  private Price(int originalPrice, int reducedPrice, double reducedPriceInEuro, String typeOfSale) {
    this.originalPrice = originalPrice;
    this.reducedPrice = reducedPrice;
    this.reducedPriceInEuro = reducedPriceInEuro;
    this.typeOfSale = typeOfSale;
  }

  public int getOriginalPrice() {
    return originalPrice;
  }

  public int getReducedPrice() {
    return reducedPrice;
  }

  public String getTypeOfSale() {
    return typeOfSale;
  }

  public double getReducedPriceInEuro() {
    return reducedPriceInEuro;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Price price = (Price) o;

    if (originalPrice != price.originalPrice) {
      return false;
    }
    if (reducedPrice != price.reducedPrice) {
      return false;
    }
    if (Double.compare(price.reducedPriceInEuro, reducedPriceInEuro) != 0) {
      return false;
    }
    return typeOfSale != null ? typeOfSale.equals(price.typeOfSale) : price.typeOfSale == null;
  }

  @Override
  public int hashCode() {
    int result;
    long temp;
    result = originalPrice;
    result = 31 * result + reducedPrice;
    result = 31 * result + (typeOfSale != null ? typeOfSale.hashCode() : 0);
    temp = Double.doubleToLongBits(reducedPriceInEuro);
    result = 31 * result + (int) (temp ^ (temp >>> 32));
    return result;
  }

  @Override
  public String toString() {
    return "Price{" +
        "originalPrice=" + originalPrice +
        ", reducedPrice=" + reducedPrice +
        ", typeOfSale='" + typeOfSale + '\'' +
        ", reducedPriceInEuro=" + reducedPriceInEuro +
        '}';
  }

  public static class PriceBuilder {

    private int originalPrice;
    private int reducedPrice;
    private String typeOfSale;
    private double reducedPriceInEuro;

    public PriceBuilder originalPrice(int originalPrice) {
      this.originalPrice = originalPrice;
      return this;
    }

    public PriceBuilder reducedPrice(int reducedPrice) {
      this.reducedPrice = reducedPrice;
      return this;
    }

    public PriceBuilder typeOfSale(String typeOfSale) {
      this.typeOfSale = typeOfSale;
      return this;
    }

    public  PriceBuilder reducedPriceInEuro(double reducedPriceInEuro){
      this.reducedPriceInEuro = reducedPriceInEuro;
      return this;
    }

    public Price build() {
      return new Price(originalPrice, reducedPrice,reducedPriceInEuro, typeOfSale);
    }
  }
}
